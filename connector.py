import paho.mqtt.client as mqtt
import datetime
import json

class Gateway():

    def __init__(self):
        self.client = mqtt.Client()
        self.broker_url = '192.168.43.177'
        self.broker_port = 1882

    def connect(self):
        self.client.connect(self.broker_url, self.broker_port, 60)

    def loop_forever(self):
        self.client.loop_forever()

    def format_value(self, value):
        ts = datetime.datetime.now().timestamp()
        payload = {'timestamp': ts, 'value': value}
        return json.dumps(payload)

    def publish(self, topic: str, payload: str):
        self.client.publish(topic=topic, payload=payload, qos=1)

    def autopublish(self, topic: str):
        def decorator(func):
            def wrapper(value):
                payload = func(value)
                self.publish(topic, payload)
                return payload
            return wrapper
        return decorator

    def subscribe(self, topic: str, handler):
        self.client.subscribe(topic, qos=1)
        self.client.message_callback_add(topic, handler)