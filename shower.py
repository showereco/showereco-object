from lib.sensors import Retriever, FlowMeter, Button, Thermo
from lib.actuators import EcoScreen, RGBScreen
from connector import Gateway
import apis.LcdScreenAPI as LcdAPI
import apis.EcoScreenAPI as EcoAPI
import asyncio
import signal


flow_meter = FlowMeter()
play_button = Button()
left_button = Button(3)
right_button = Button(4)
thermo = Thermo()

retriever = Retriever()
gateway = Gateway()

@retriever.task(sensor = flow_meter, interval = 1.0)
@gateway.autopublish(topic='shower/sensors/flowmeter')
def read_flow(flow):
    print('flow : ', flow)
    return gateway.format_value(flow)

@retriever.task(sensor = play_button, interval = 0.5)
@gateway.autopublish(topic='shower/sensors/play')
def read_state(state):
    print('play : ', state)
    return gateway.format_value(state)

@retriever.task(sensor = left_button, interval = 0.5)
@gateway.autopublish(topic='shower/sensors/left')
def read_state(state):
    print('left : ', state)
    return gateway.format_value(state)

@retriever.task(sensor = right_button, interval = 0.5)
@gateway.autopublish(topic='shower/sensors/right')
def read_state(state):
    print('right : ', state)
    return gateway.format_value(state)


@retriever.task(sensor = thermo, interval = 5.0)
@gateway.autopublish(topic='shower/sensors/thermo')
def read_temp(temp):
    print('temp : ', temp)
    return gateway.format_value(temp)


screen = EcoScreen()
testScreen = RGBScreen()

screen.draw_main_text('5 l/m')
testScreen.setBackground(255, 0, 0)
testScreen.setText('Test')


try:
    gateway.connect()
    LcdAPI.subscribe_to_gateway(gateway)
    EcoAPI.subscribe_to_gateway(gateway)
   #gateway.loop_forever()

    retriever.run()
except KeyboardInterrupt:
    flow_meter.cleanup(), play_button.cleanup(), left_button.cleanup(), right_button.cleanup()
    retriever.close()





