from lib.actuators import ActuatorBase
import lib.actuators.grove_rgb_lcd as lcd

class RGBScreen(ActuatorBase):

    def __init__(self):
        super().__init__()

    def setBackground(self, r: int, g: int, b: int):
        lcd.setRGB(r, g, b)

    def setText(self, txt: str):
        lcd.setText(txt)
