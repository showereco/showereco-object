from lib.actuators import ActuatorBase
from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import sh1106
from PIL import ImageFont, ImageDraw

class EcoScreen(ActuatorBase):

    def __init__(self):
        super().__init__()
        self.font =ImageFont.truetype('./assets/fonts/Roboto-Regular.ttf',50)
        serial = i2c(port=1, address=0x3C)
        self.device = sh1106(serial, rotate=0)

    def draw_main_text(self, text: str):
        with canvas(self.device) as draw:
            draw.text((10, 10), text, font=self.font, fill="white")


