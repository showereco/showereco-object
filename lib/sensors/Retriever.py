from lib.sensors import SensorEntity
import asyncio

class Retriever:
    
    def __init__(self, main_loop = False):
        if main_loop : self.loop = asyncio.get_event_loop()
        else: self.loop = asyncio.new_event_loop()

    def task(self, sensor: SensorEntity, interval: float = 1.0):
        return sensor.loop_start(self.loop, interval=interval)

    def run(self):
        self.loop.run_forever()

    def stop(self):
        self.loop.stop()
    
    def close(self):
        self.stop()
        self.loop.close()

    def __del__(self):
        self.close()