from lib.sensors import SensorEntity
import RPi.GPIO as GPIO
import asyncio

class FlowMeter(SensorEntity):

    def __init__(self, pin: int = 11):
        super().__init__()
        self.pin = pin
        self.rate = 0
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.pin, GPIO.IN)
        GPIO.add_event_detect(self.pin, GPIO.FALLING, callback=self.handle_pulse, bouncetime=10)

    def handle_pulse(self, pin):
        if self.loop: self.rate+=1

    def read_state(self):
        return None

    async def _run(self, loop, interval, handler):
        async def async_handler(state): handler(state)
        self.loop = True
        while self.loop:
            flow = float(self.rate/interval)/7.5
            self.rate = 0
            loop.create_task(async_handler(flow))
            await asyncio.sleep(interval)

    def loop_stop(self):
        super().loop_stop()
        self.rate = 0

    def cleanup(self):
        super().__init__()
        GPIO.cleanup()