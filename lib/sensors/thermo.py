from w1thermsensor import W1ThermSensor
from lib.sensors import SensorEntity

class Thermo(SensorEntity):
    
    def __init__(self):
        self.sensor = W1ThermSensor()

    def read_state(self):
        try:
            return self.sensor.get_temperature()
        except:
            return -1
    