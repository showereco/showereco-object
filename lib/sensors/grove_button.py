from lib import grovepi
from lib.sensors import SensorEntity


class Button(SensorEntity):

    def __init__(self, pin: int = 2):
        super().__init__()
        self.pin = pin
        grovepi.pinMode(pin, "INPUT")

    def read_state(self):
        return grovepi.digitalRead(self.pin)