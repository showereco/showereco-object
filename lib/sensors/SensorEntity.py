from abc import ABC, abstractmethod
import asyncio

class SensorEntity(ABC):

    def __init__(self):
        self.loop = False

    async def _run(self, loop, interval, handler):
        async def async_handler(state): handler(state)
        self.loop = True
        while self.loop:
            loop.create_task(async_handler(self.read_state()))
            await asyncio.sleep(interval)

    def loop_stop(self):
        self.loop = False

    def loop_start(self, loop, interval: float = 1.0):
        def wrapper(handler):
            loop.create_task(self._run(loop, interval, handler))
            return handler
        return wrapper

    @abstractmethod
    def read_state(self):
        pass

    def cleanup(self):
        print("Cleanup sensor")
        self.loop_stop()
    
    def __del__(self):
        self.cleanup()

    