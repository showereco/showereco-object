from lib.actuators import EcoScreen

screen = EcoScreen()
main_topic = 'shower/actuators/eco/'


def update_text(client, userdata, message):
    screen.draw_main_text(str(message.payload.decode("utf-8")))

def subscribe_to_gateway(gateway):
    gateway.subscribe(main_topic + 'text', update_text)