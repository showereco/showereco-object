from lib.actuators import RGBScreen

screen = RGBScreen()
main_topic = 'shower/actuators/lcd/'

def background_change(client, userdata, message):
    colors = message.payload.decode("utf-8")
    if hasattr(colors, "r") and hasattr(colors, "g") and hasattr(colors, "b"):
        screen.setBackground(int(colors.r), int(colors.g), int(colors.b))

def update_text(client, userdata, message):
    screen.setText(str(message.payload.decode("utf-8")))

def subscribe_to_gateway(gateway):
    gateway.subscribe(main_topic + 'background', background_change)
    gateway.subscribe(main_topic + 'text', update_text)